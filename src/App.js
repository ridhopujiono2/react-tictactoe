import * as React from "react";
import { createContext, useContext, useState } from "react";

// Initialize game context
const GameContext = createContext();

// Make component Board
function Board() {
  // Spread game context
  const { squares, selectSquare, status, winnerSquare } =
    useContext(GameContext);

  // Rendering squre
  function renderSquare(i) {
    const isWinnerSquare = winnerSquare.includes(i);
    const classWinner = isWinnerSquare ? "bg-emerald-700" : "";
    return (
      <button
        className={`square w-20 h-20 m-2 border ${
          squares[i] === "X" ? `text-yellow-300` : `text-pink-500`
        } text-3xl font-bold ${classWinner}`}
        onClick={() => selectSquare(i)}
      >
        {squares[i]}
      </button>
    );
  }

  return (
    <div>
      <div>{status}</div>
      <div className="flex">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="flex">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="flex">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>
  );
}

// Make component game
function Game() {
  const [squares, setSquares] = useState(Array(9).fill(null));
  const [nextValue, setNextValue] = useState("X");
  const [winner, setWinner] = useState(null);
  const [winnerSquare, setWinnerSquare] = useState(Array(9).fill(false));

  function selectSquare(square) {
    if (winner || squares[square]) {
      return;
    }
    const squaresCopy = [...squares];
    squaresCopy[square] = nextValue;
    setSquares(squaresCopy);
    setNextValue(calculateNextValue(squaresCopy));
    setWinner(calculateWinner(squaresCopy));
    const winnerResult = calculateWinner(squaresCopy);
    if (winnerResult) {
      setWinner(winnerResult.winner);
      setWinnerSquare(winnerResult.squares);
    }
  }

  function restart() {
    setSquares(Array(9).fill(null));
    setNextValue("X");
    setWinner(null);
    setWinnerSquare(Array(9).fill(null));
  }
  function calculateStatus() {
    return winner
      ? `Winner: ${winner}`
      : squares.every(Boolean)
      ? `Match Draw`
      : `Next player: ${nextValue}`;
  }
  const gameData = {
    squares,
    selectSquare,
    restart,
    winner,
    winnerSquare,
    nextValue,
    status: calculateStatus(),
  };

  return (
    <GameContext.Provider value={gameData}>
      <div className="flex flex-col items-center justify-center text-slate-50 bg-emerald-500 h-screen text-center">
        <div>
          <Board />
          <button
            onClick={restart}
            className="bg-slate-50 px-4 py-2 mt-2 text-slate-900 rounded"
          >
            restart
          </button>
        </div>
      </div>
    </GameContext.Provider>
  );
}

function calculateNextValue(squares) {
  return squares.filter(Boolean).length % 2 === 0 ? "X" : "O";
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return { winner: squares[a], squares: lines[i] };
    }
  }
  return null;
}

function App() {
  return <Game />;
}

export default App;
